"use strict";
const nunjucks = require('nunjucks');
const path = require('path');
class App {
    constructor(express, isDev) {
        this.express = express;
        this.isDev = isDev;
        this.middlewares();
        this.views();
        this.routes();
    }
    middlewares() {
        this.express.use(express.urlenconded({ extended: false }));
    }
    views() {
        nunjucks.configure(path.resolve(__dirname, 'app', 'views'), {
            watch: this.isDev,
            express: this.express,
            autoescape: true
        });
        this.express.set('view engine', 'njk');
    }
    routes() {
    }
}
module.exports = App;
