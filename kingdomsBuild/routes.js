"use strict";
const exp = require('express');
const routes = exp.Router();
const UserControl = require('./src/app/controllers/UserController');
routes.get('/signup', (req, res) => UserControl.create(req, res));
routes.post('/signup', (req, res) => UserControl.insert(req, res));
module.exports = routes;
