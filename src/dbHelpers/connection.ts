import { response } from "express";

const mongoose = require('mongoose')

const connect = () => {
    mongoose.connect('mongodb://localhost:27017/T10SSRTEMPLATE', {useNewUrlParser: true})
    .then(
        () => { console.log('DB connection worked like a charm!') }, 
        (err: any) => console.log(`Something went wrong, here goes a tip from our's realm's detective ${err}`))
}
module.exports = connect