const User = require('../../../models/UserModel')
var mongoose = require('mongoose')

class UserController {
    create (req: any, res: any) {
        return res.render('auth/signup')
    }

    insert = async (req: any, res: any) => {
        const new_user = await new User({...req.body, _id: new mongoose.Types.ObjectId()})
        const response = await new_user.save()
        return response
   
    }
}

module.exports = new UserController()