const exp = require('express')

const routes = exp.Router()

const UserControl = require('./src/app/controllers/UserController')

routes.get('/signup', (req: any, res: any) => UserControl.create(req, res))
routes.post('/signup', (req: any, res: any) => UserControl.insert(req, res))
module.exports = routes