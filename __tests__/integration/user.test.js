const User = require('../../kingdomsBuild/src/app/controllers/UserController')
const mongoose = require('mongoose')
const app = require('../../kingdomsBuild/server')

describe('Register', () => {
    it('should register a new user', async () => {
        const response = await User.insert({
            body: {
                _id: new mongoose.Types.ObjectId(),
                email: 'testing@gmail.com',
                password_hash: '123*&3332'
             }
        }, null)

        expect(response.email).toBe('testing@gmail.com')
    })
})