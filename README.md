# T10LAB - SSR Template

All right, fellows, this is a server-side rendering architecture template base. I hope we can use this to save time in our Kingdom's jobs!
Be free to use as you want, this is also a part of me inside this huge family we call community.

# OBS : After I finish this project I'm gonna create a playlist with the entire Tutorial!

# First of all, let me show you guys how this should work!

## We're gonna use Yarn
## Let's TypeScript it
    We need to focus on our data type, using TypeScript makes it easier. We can detect errors a lot faster and save some time in the future. 
## MongoDB
    It's my first and favorite NoSQL database, just by using mongoose makes me smile! By the way, I choosing a NoSQL database because I have no idea of what kind of projects we're gonna use this template in, and a NoSQL DB may not be the best choice ever, but it will always do its job!
## Jest for sure!
    Testing is never too much! We're gonna use Jest in order to test our functions, it's my first time doing this but I'll do my best to create a useful and durable structure.
## Nunjucks    
## Heroku for hosting
    Because it's free, haha just kidding, I really enjoy working with Heroku, its very intuitive and its gonna do the job.

This document (Yeah, this Readme you're reading now), still in dev stage, so don't worry about some lack of info.    
    