const user_mongoose = require('mongoose')

const user_schema = user_mongoose.Schema({
    _id: user_mongoose.Schema.ObjectId, 
    password_hash: String,
    email: String    
})

module.exports = user_mongoose.model('User', user_schema)