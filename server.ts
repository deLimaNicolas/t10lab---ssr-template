const nunjucks = require('nunjucks')
const path = require('path')
const express = require('express')
const router = require('./routes')
const db_connect = require('./src/dbHelpers/connection')

class App {
      express : any;
      isDev : boolean;

      constructor () {
          this.express = express()
          this.isDev = process.env.NODE_ENV !== 'production'

          this.middlewares()
          this.views()
          this.routes()
      }

      middlewares () {
        this.express.use(express.urlencoded({ extended: false }))
        db_connect()
      } 

      views () {
        nunjucks.configure(path.resolve('src', 'app', 'views'), {
            watch : this.isDev,
            express : this.express,
            autoescape : true
        })

        this.express.use(express.static(path.resolve('src', 'public')))
        this.express.set('view engine', 'njk')
      }

      routes () {
        this.express.use(router)
      }
}

module.exports = new App().express